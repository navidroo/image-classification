# Jupyter notebooks

## Main:

 * [analyze\_folder.ipynb](analyze_folder.ipynb): create indices linking notebooks to data files
 * [overview.ipynb](overview.ipynb): notebook illustrating (most) of the overall process (renamed from **sanity\_check\_debevev.ipynb**)

## Contents of Folders:

 * [chemiluminescence](chemiluminescence): notebooks illustrating chemiluminescence / extraction of flames

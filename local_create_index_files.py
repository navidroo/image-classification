#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""This file creates index files needed by jupyter notebooks.

A copy resides in <...>/microfit/bin/create_index_files.py"""

import argparse
import os
import warnings
from microfit.process import create_index_files

warnings.filterwarnings(action='once')

parser = argparse.ArgumentParser()
parser.add_argument(
    'data_path', help='path to folder containing measurement cases')
parser.add_argument('config', help='yaml configuration file')
parser.add_argument('-v', '--verbosity', action='count',
                    help='verbosity level of output')
parser.add_argument('--hdr_previews', action='count',
                    help='create png versions of hdr images')
parser.add_argument('--raw_index', action='count',
                    help='save raw index')


if __name__ == '__main__':
    """Parse/check arguments and invoke main."""

    args = parser.parse_args()

    data_path, config_file = args.data_path, args.config
    kwargs = {'hdr_previews': args.hdr_previews is not None,
              'raw_index': args.raw_index is not None,
              'verbosity': args.verbosity}

    create_index_files(data_path, config_file, **kwargs)
